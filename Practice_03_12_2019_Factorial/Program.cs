﻿using System;
using System.Numerics;

namespace Practice_03_12_2019_Factorial
{
    class Program
    {
        static int Main(string[] args)
        {
            int number = int.Parse(args[0]);

            return Factorial(number);
        }
        static int Factorial(int number)
        {
            if (number > 1)            
                return number * Factorial(number - 1);           
            else
                return 1;
        }
    }
}
