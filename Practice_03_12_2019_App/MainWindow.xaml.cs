﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows;
using System.Windows.Input;

namespace Practice_03_12_2019_App
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        const int REQUEIRED_NUMBER_COUNT = 10;
        public MainWindow()
        {
            InitializeComponent();
        }

        private void RunButtonClick(object sender, RoutedEventArgs e)
        {
            List<int> numbers = new List<int>();
            string numbersStr = numberBox.Text;
            WeakReference reference;

            try
            {
                numbers = numbersStr.Split(' ').Select(number => Convert.ToInt32(number)).ToList();
            }
            catch (Exception exception)
            {
                MessageBox.Show($"Ошибка! \n{exception.Message}");
                return;
            }
            if (!numbers.Any(number => number < 0))
            {
                if (numbers.Count == REQUEIRED_NUMBER_COUNT)
                {
                    foreach (int number in numbers)
                    {
                        var result = FactorialProcess(out reference, number);

                        for(int i = 0; reference.IsAlive && i < 10; i++)
                        {
                            GC.Collect();
                            GC.WaitForPendingFinalizers();
                        }

                        MessageBox.Show(result.ToString());
                    }
                }
                else
                {
                    MessageBox.Show($"Чисел {numbers.Count}/{REQUEIRED_NUMBER_COUNT}");
                }
            }
            else
            {
                MessageBox.Show("Найдено отрицательное число!");
            }

            return;
        }

        private int FactorialProcess(out WeakReference weakReference, int number)
        {
            var assemblyPath = System.Reflection.Assembly.GetEntryAssembly().Location;
            var context = new FactorialAssemblyLoadContext();
            weakReference = new WeakReference(context, true);
            //Не учел случай если .dll файл будет в другом месте
            try
            {
                assemblyPath = assemblyPath.Replace("Practice_03_12_2019_App", "Practice_03_12_2019_Factorial");
            }
            catch (Exception exception)
            {
                MessageBox.Show($"Ошибка! \n{exception.Message}");
                return -1;
            }
            var factorialAssembly = context.LoadFromAssemblyPath(assemblyPath);

            var args = new object[1] { new string[] { number.ToString() } };
            int result = (int)factorialAssembly.EntryPoint.Invoke(null, args);
            context.Unload();

            return result;
        }
    }
}
